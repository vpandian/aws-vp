echo "Set Server 3x EKS and Nomad to 2"
aws autoscaling update-auto-scaling-group --auto-scaling-group-name c5n-3x-nomad-asg --desired-capacity 0 --max-size 0 --min-size 0
aws autoscaling update-auto-scaling-group --auto-scaling-group-name eks-9abde107-e111-0f72-5a9c-97cfe02d0155 --desired-capacity 0 --max-size 0 --min-size 0
echo "Start EC2 Server Runner Instance"
aws ec2 stop-instances --instance-ids i-00bc9281f2757daaf
