ssh -i $US_WEST_PEM_FILE $SERVICES_SSH_HOSTNAME \<< EOF
#commands to run on remote EC2
replicatedctl app stop || true
sleep 10
sudo service replicated-operator stop || true
sleep 10
sudo service replicated-ui stop || true
sleep 10
sudo service replicated stop || true
sleep 10
sudo service replicated start || true
sleep 10
sudo service replicated-ui start || true
sleep 10
sudo service replicated-operator start || true
sleep 10
sudo service replicated status || true
sleep 10
replicatedctl app start || true
sleep 10
EOF
