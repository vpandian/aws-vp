echo "Set 2x Nomad client"
aws autoscaling update-auto-scaling-group --auto-scaling-group-name vijay-2x-nomad-clients-test --desired-capacity 2 --max-size 2 --min-size 2
echo "start 2x Service EC2 Instance"
aws ec2 start-instances --instance-ids i-0bff304196c50651c
